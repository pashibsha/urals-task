import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';

import { OrderController } from 'src/controllers/order.controller';

import { Order } from 'src/models/order.model';

import { OrderService } from 'src/services/order.service';

import { BooksModule } from './book.module';

@Module({
  imports: [SequelizeModule.forFeature([Order]), BooksModule],
  controllers: [OrderController],
  providers: [OrderService],
})
export class OrdersModule {}
