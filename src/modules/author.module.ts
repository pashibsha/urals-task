import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';

import { AuthorController } from 'src/controllers/author.controller';

import { Author } from 'src/models/author.model';

import { AuthorService } from 'src/services/author.service';

@Module({
  imports: [SequelizeModule.forFeature([Author])],
  controllers: [AuthorController],
  providers: [AuthorService],
})
export class AuthorsModule {}
