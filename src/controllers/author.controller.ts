import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';

import { CreateAuthorDto } from 'src/dto/createAuthor.dto';

import { AuthorService } from 'src/services/author.service';

@Controller('author')
export class AuthorController {
  constructor(private authorService: AuthorService) {}

  @Post()
  async create(@Body() createAuthorDto: CreateAuthorDto) {
    return this.authorService.create(createAuthorDto);
  }

  @Put(':authorId')
  async update(
    @Param('authorId') authorId: string,
    @Body() updateAuthorDto: CreateAuthorDto,
  ) {
    return this.authorService.update(authorId, updateAuthorDto);
  }

  @Get()
  async findAll() {
    return this.authorService.findAll();
  }

  @Get(':authorId')
  async findOne(@Param('authorId') authorId: string) {
    return this.authorService.findOne(authorId);
  }

  @Delete(':authorId')
  async remove(@Param('authorId') authorId: string) {
    return this.authorService.remove(authorId);
  }
}
