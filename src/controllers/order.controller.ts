import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';

import { CreateOrderDto } from 'src/dto/createOrder.dto';

import { OrderService } from 'src/services/order.service';

@Controller('order')
export class OrderController {
  constructor(private orderService: OrderService) {}

  @Post()
  async create(@Body() createOrderDto: CreateOrderDto) {
    return this.orderService.create(createOrderDto);
  }

  @Put(':orderId')
  async update(
    @Param('orderId') orderId: string,
    @Body() updateOrderDto: CreateOrderDto,
  ) {
    return this.orderService.update(orderId, updateOrderDto);
  }

  @Get()
  async findAll() {
    return this.orderService.findAll();
  }

  @Get(':orderId')
  async findOne(@Param('orderId') orderId: string) {
    return this.orderService.findOne(orderId);
  }

  @Delete(':orderId')
  async remove(@Param('orderId') orderId: string) {
    return this.orderService.remove(orderId);
  }

  @Get('my-order/:userId')
  async getMyOrder(@Param('userId') userId: string) {
    return this.orderService.getMyOrder(userId);
  }
}
