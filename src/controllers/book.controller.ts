import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';

import { CreateBookDto } from 'src/dto/createBook.dto';

import { BookService } from 'src/services/book.service';

@Controller('book')
export class BookController {
  constructor(private bookService: BookService) {}

  @Post()
  async create(@Body() createBookDto: CreateBookDto) {
    return this.bookService.create(createBookDto);
  }

  @Put(':bookId')
  async update(
    @Param('bookId') bookId: string,
    @Body() updateBookDto: CreateBookDto,
  ) {
    return this.bookService.update(bookId, updateBookDto);
  }

  @Get()
  async findAll() {
    return this.bookService.findAll();
  }

  @Get(':bookId')
  async findOne(@Param('bookId') bookId: string) {
    return this.bookService.findOne(bookId);
  }

  @Delete(':bookId')
  async remove(@Param('bookId') bookId: string) {
    return this.bookService.remove(bookId);
  }

  @Get('by-author/:authorId')
  async findByAuthor(@Param('authorId') authorId: string) {
    return await this.bookService.findByAuthor(authorId);
  }

  @Get()
  async findByTitle(@Query('title') title: string) {
    return await this.bookService.findByTitle(title);
  }
}
