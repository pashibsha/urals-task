export class CreateAuthorDto {
  name: string;
  nickname: string;
  birth: string;
}
