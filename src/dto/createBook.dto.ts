import { Author } from 'src/models/author.model';
import { User } from 'src/models/user.model';

export class CreateBookDto {
  title: string;
  price: string;
  authorid: string;
}
