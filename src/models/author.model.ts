import { Column, DataType, HasMany, Model, Table } from 'sequelize-typescript';

import { Book } from './book.model';

@Table({ tableName: 'authors', timestamps: false })
export class Author extends Model {
  @Column({
    primaryKey: true,
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
  })
  id: string;

  @Column({ type: DataType.STRING })
  name: string;

  @Column({ type: DataType.STRING })
  nickname: string;

  @Column({ type: DataType.STRING })
  birth: string;

  @HasMany(() => Book)
  books: Book[];
}
