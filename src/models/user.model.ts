import {
  BelongsToMany,
  Column,
  DataType,
  Model,
  Table,
} from 'sequelize-typescript';

import { Book } from './book.model';
import { Order } from './order.model';

@Table({ tableName: 'users' })
export class User extends Model {
  @Column({
    primaryKey: true,
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
  })
  id: string;

  @Column({ type: DataType.STRING })
  name: string;

  @Column({ type: DataType.STRING })
  phoneNumber: string;

  @Column({ type: DataType.STRING })
  email: string;

  @BelongsToMany(() => Book, () => Order)
  books: Book[];
}
