import {
  Column,
  DataType,
  ForeignKey,
  Model,
  Table,
} from 'sequelize-typescript';

import { Book } from './book.model';
import { User } from './user.model';

@Table({ tableName: 'orders' })
export class Order extends Model {
  @Column({
    primaryKey: true,
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
  })
  id: string;

  @ForeignKey(() => Book)
  @Column
  bookId: string;

  @ForeignKey(() => User)
  @Column
  userId: string;

  @Column({ type: DataType.STRING })
  totalPrice: string;
}
