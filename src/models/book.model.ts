import {
  BelongsTo,
  BelongsToMany,
  Column,
  DataType,
  ForeignKey,
  Model,
  Table,
} from 'sequelize-typescript';

import { Author } from './author.model';
import { User } from './user.model';
import { Order } from './order.model';

@Table({ tableName: 'books', timestamps: false })
export class Book extends Model {
  @Column({
    primaryKey: true,
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
  })
  id: string;

  @Column({ type: DataType.STRING })
  title: string;

  @Column({ type: DataType.STRING })
  price: string;

  @ForeignKey(() => Author)
  @Column({ type: DataType.UUID })
  authorId: string;

  @BelongsTo(() => Author)
  author: Author;

  @BelongsToMany(() => User, () => Order)
  users: User[];
}
