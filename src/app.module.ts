import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { SequelizeModule } from '@nestjs/sequelize';

import { User } from './models/user.model';
import { Order } from './models/order.model';
import { Author } from './models/author.model';
import { Book } from './models/book.model';

import { OrdersModule } from './modules/order.module';
import { UsersModule } from './modules/user.module';
import { AuthorsModule } from './modules/author.module';
import { BooksModule } from './modules/book.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
    }),
    SequelizeModule.forRoot({
      dialect: 'postgres',
      host: process.env.POSTGRES_HOST,
      port: Number(process.env.POSTGRES_PORT),
      username: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB,
      models: [User, Order, Author, Book],
      synchronize: true,
      autoLoadModels: true,
    }),
    OrdersModule,
    UsersModule,
    AuthorsModule,
    BooksModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
