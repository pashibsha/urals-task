import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';

import { Book } from 'src/models/book.model';
import { Order } from 'src/models/order.model';

import { CreateOrderDto } from 'src/dto/createOrder.dto';

import { BookService } from './book.service';

@Injectable()
export class OrderService {
  constructor(
    @InjectModel(Order) private orderRepository: typeof Order,
    private readonly bookService: BookService,
  ) {}

  async create(createOrderDto: CreateOrderDto): Promise<Order> {
    return await this.orderRepository.create({ ...createOrderDto });
  }

  async update(
    orderId: string,
    updateOrderDto: CreateOrderDto,
  ): Promise<[affectedCount: number, affectedRows: Order[]]> {
    return await this.orderRepository.update(updateOrderDto, {
      where: { id: orderId },
      returning: true,
    });
  }

  async findAll(): Promise<Order[]> {
    return await this.orderRepository.findAll();
  }

  async findByUserId(userId: string): Promise<Order[]> {
    return await this.orderRepository.findAll({ where: { userId: userId } });
  }

  async findOne(orderId: string): Promise<Order> {
    return await this.orderRepository.findOne({ where: { id: orderId } });
  }

  async remove(orderId: string): Promise<number> {
    return this.orderRepository.destroy({ where: { id: orderId } });
  }

  async getMyOrder(userId: string): Promise<String> {
    let totalPrice: number = 0;
    let books: Book[] = [];

    return this.findByUserId(userId).then((orders) =>
      Promise.all(
        orders.map((order) =>
          this.bookService.findOne(order.bookId).then((res) => {
            const book: Book = res;

            books.push(book);
            totalPrice += Number(book.price);
          }),
        ),
      ).then(() => {
        return JSON.stringify({
          user: userId,
          books: books,
          booksAmount: books.length,
          totalPrice: totalPrice,
        });
      }),
    );
  }
}
