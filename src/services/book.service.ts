import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';

import { Author } from 'src/models/author.model';
import { Book } from 'src/models/book.model';

import { CreateBookDto } from 'src/dto/createBook.dto';

@Injectable()
export class BookService {
  constructor(@InjectModel(Book) private bookRepository: typeof Book) {}

  async create(createBookDto: CreateBookDto): Promise<Book> {
    return await this.bookRepository.create({ ...createBookDto });
  }

  async update(
    bookId: string,
    updateBookDto: CreateBookDto,
  ): Promise<[affectedCount: number, affectedRows: Book[]]> {
    return await this.bookRepository.update(updateBookDto, {
      where: { id: bookId },
      returning: true,
    });
  }

  async findAll(): Promise<Book[]> {
    return await this.bookRepository.findAll({ include: Author });
  }

  async findOne(bookId: string): Promise<Book> {
    return await this.bookRepository.findOne({
      where: { id: bookId },
      include: Author,
    });
  }

  async remove(bookId: string): Promise<number> {
    return this.bookRepository.destroy({ where: { id: bookId } });
  }

  async findByAuthor(authorId: string): Promise<Book[]> {
    return await this.bookRepository.findAll({ where: { authorId: authorId } });
  }

  async findByTitle(title: string): Promise<Book> {
    title.replace('_', ' ');
    return await this.bookRepository.findOne({ where: { title: title } });
  }
}
