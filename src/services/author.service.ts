import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';

import { CreateAuthorDto } from 'src/dto/createAuthor.dto';

import { Author } from 'src/models/author.model';

@Injectable()
export class AuthorService {
  constructor(@InjectModel(Author) private authorsRepository: typeof Author) {}

  async create(createAuthorDto: CreateAuthorDto): Promise<Author> {
    return await this.authorsRepository.create({
      ...createAuthorDto,
      books: [],
    });
  }

  async update(
    authorId: string,
    updateAuthorDto: CreateAuthorDto,
  ): Promise<[affectedCount: number, affectedRows: Author[]]> {
    return await this.authorsRepository.update(updateAuthorDto, {
      where: { id: authorId },
      returning: true,
    });
  }

  async findAll(): Promise<Author[]> {
    return await this.authorsRepository.findAll();
  }

  async findOne(authorId: string): Promise<Author> {
    return await this.authorsRepository.findOne({ where: { id: authorId } });
  }

  async remove(authorId: string): Promise<number> {
    return this.authorsRepository.destroy({ where: { id: authorId } });
  }
}
