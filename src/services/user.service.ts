import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';

import { CreateUserDto } from 'src/dto/createUser.dto';

import { User } from 'src/models/user.model';

@Injectable()
export class UserService {
  constructor(@InjectModel(User) private userRepository: typeof User) {}

  async create(createUserDto: CreateUserDto): Promise<User> {
    return await this.userRepository.create({ ...createUserDto });
  }

  async update(
    userId: string,
    updateUserDto: CreateUserDto,
  ): Promise<[affectedCount: number, affectedRows: User[]]> {
    return await this.userRepository.update(updateUserDto, {
      where: { id: userId },
      returning: true,
    });
  }

  async findAll(): Promise<User[]> {
    return (await this.userRepository.findAll()) || [];
  }

  async findOne(userId: string): Promise<User> {
    return await this.userRepository.findOne({ where: { id: userId } });
  }

  async remove(userId: string): Promise<number> {
    return this.userRepository.destroy({ where: { id: userId } });
  }
}
